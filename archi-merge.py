from ArchiModel import ArchiModel
import os

# If the master model already exists, then delete it
master_merge_filename = ".\\master_merge.archimate"
if os.path.exists(master_merge_filename):
    os.remove(master_merge_filename)

# Create models
dest_model = ArchiModel("Master merged")
dest_model.createBlankModel()
source_model = ArchiModel()

# For any *.architmate files in the current directory, merge them into this model
for f in os.listdir("."):
    if f.endswith(".archimate"):
        source_model.load(f)
        dest_model.mergeModel(source_model)
        dest_model.mergeRelations(source_model)

#
# Save the master model
#
dest_model.save(master_merge_filename)
