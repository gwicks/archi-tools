from xml.etree import ElementTree as ElementTree
import uuid

# Usage:
# from ArchiModel import ArchiModel
#
# Notes:
# Dec 2018: Not complete merge of model concept attributes
#           Does not merge views
#
# ToDo:
# - It would be nice if the merge also 'organised' the objects into some sub-folder structure.
#

class ArchiModel:

    ns = {'archimate': 'http://www.opengroup.org/xsd/archimate',
        'xsi': 'http://www.w3.org/2001/XMLSchema-instance',
        'archi': 'http://www.archimatetool.com/archimate'}

    concept2folder_dict = {
        # Strategy
        "Resource": "strategy",
        "Capability": "strategy",
        "CourseOfAction": "strategy",

        # Business
        "BusinessActor": "business",
        "BusinessRole": "business",
        "BusinessCollaboration": "business",
        "BusinessInterface": "business",
        "BusinessProcess": "business",
        "BusinessFunction": "business",
        "BusinessInteraction": "business",
        "BusinessEvent": "business",
        "BusinessService": "business",
        "BusinessObject": "business",
        "Contract": "business",
        "Representation": "business",
        "Product": "business",

        # Application
        "ApplicationCollaboration": "application",
        "ApplicationComponent": "application",
        "ApplicationEvent": "application",
        "ApplicationFunction": "application",
        "ApplicationInteraction": "application",
        "ApplicationInterface": "application",
        "ApplicationProcess": "application",
        "ApplicationService": "application",
        "DataObject": "application",

        # Technology and facilities
        "Node": "technology",
        "Device": "technology",
        "SystemSoftware": "technology",
        "TechnologyCollaboration": "technology",
        "TechnologyInterface": "technology",
        "Path": "technology",
        "CommunicationNetwork": "technology",
        "TechnologyFunction": "technology",
        "TechnologyProcess": "technology",
        "TechnologyInteraction": "technology",
        "TechnologyEvent": "technology",
        "TechnologyService": "technology",
        "Artifact": "technology",
        "Equipment": "technology",
        "Facility": "technology",
        "DistributionNetwork": "technology",
        "Material": "technology",

        # Motivation
        "Stakeholder": "motivation",
        "Driver": "motivation",
        "Assessment": "motivation",
        "Goal": "motivation",
        "Outcome": "motivation",
        "Principle": "motivation",
        "Requirement": "motivation",
        "Constraint": "motivation",
        "Meaning": "motivation",
        "Value": "motivation",

        # Implementation
        "WorkPackage": "implementation_migration",
        "Deliverable": "implementation_migration",
        "ImplementationEvent": "implementation_migration",
        "Plateau": "implementation_migration",
        "Gap": "implementation_migration",

        # Other
        "Location": "other",
        "Grouping": "other",
        "Junction": "other"
    }

    #
    #
    #
    def concept2folder(self, concept):
        return self.concept2folder_dict[concept]

    #
    #
    #
    relation2folder_dict = {
        "AggregationRelationship": "relations",
        "AccessRelationship": "relations",
        "CompositionRelationship": "relations",
        "AssociationRelationship": "relations",
        "ServingRelationship": "relations",
        "AssignmentRelationship": "relations",
        "RealizationRelationship": "relations",
        "FlowRelationship": "relations",
        "TriggeringRelationship": "relations",
        "InfluenceRelationship": "relations",
        "SpecializationRelationship": "relations"
    }

    #
    #
    #
    def relation2folder(self, relation):
        return self.relationsfolder_dict[relation]


    #
    #
    #
    def __init__(self, name="New"):
        n = ElementTree.Element("archimate:model")
        n.attrib["xmlns:xsi"] = "http://www.w3.org/2001/XMLSchema-instance"
        n.attrib["xmlns:archimate"] = "http://www.archimatetool.com/archimate"
        n.attrib["name"] = name
        n.attrib["id"] = str(uuid.uuid4())
        n.attrib["version"] = "4.0.1"
        self.model = n

    #
    #
    #
    def createBlankModel(self):
        self.addfolder("Strategy", "strategy")
        self.addfolder("Business", "business")
        self.addfolder("Application", "application")
        self.addfolder("Technology & Physical", "technology")
        self.addfolder("Motivation", "motivation")
        self.addfolder("Implementation & Migration", "implementation_migration")
        self.addfolder("Other", "other")
        self.addfolder("Relations", "relations")
        self.addfolder("Views", "diagrams")        
        self.addCoreElement("diagrams", "ArchimateDiagramModel", "Default View")

    #
    #
    #
    def load(self, filename):
        print("Loading model: " + filename)
        self.model = ElementTree.parse(filename)

    #
    #
    #
    def save(self, filename):
        ElementTree.ElementTree(self.model).write(filename)

    #
    #
    #
    def dump(self):
        ElementTree.dump(self.model)

    #
    #
    #
    def addfolder(self, name, foldertype=""):
        n = ElementTree.Element("folder")
        n.attrib["id"] = str(uuid.uuid4())
        n.attrib["name"] = name
        if foldertype:
            n.attrib["type"] = foldertype
        self.model.append(n)

    #
    #
    #
    def getId( self, id ):
        """Does this element ID exist in the model"""
        return self.model.find('.//element[@id="' + id + '"]', self.ns)

    #
    #
    #
    def getConcepts(self, conceptname):
        """Return a collection of the archimate concept type"""
        #for concept in self.model.iterfind('.//element[@xsi:type="archimate:' + conceptname + '"]', self.ns):
            #print(concept.attrib)
        return self.model.iterfind('.//element[@xsi:type="archimate:' + conceptname + '"]', self.ns)

    #
    #
    #
    def addCoreElement(self, foldertype, concept, name, id=""):
        """Add a core element to the model - i.e. to a fixed folder"""

        n = self.getId(id)
        if n is not None:
            pass

        else:
            n = ElementTree.Element("element")
            n.attrib["name"] = name
            n.attrib["xsi:type"] = "archimate:" + concept
            if id:
                n.attrib["id"] = id
            else:
                n.attrib["id"] = str(uuid.uuid1())

            f = self.model.find(".//folder[@type='" + foldertype + "']", self.ns)
            f.append(n)

        return n

    #
    # Merge a class of concepts
    #
    def mergeConcepts(self, source, concept):
        print("Merging: " + concept)
        for e in source.getConcepts(concept):
            self.addCoreElement(self.concept2folder(concept), concept, e.attrib["name"], e.attrib["id"] )
    
    #
    # Merge all the core concepts
    #
    def mergeModel(self, source):
        for concept in source.concept2folder_dict:
            self.mergeConcepts(source, concept)

    #
    #
    #
    def mergeRelations(self, source):
        print("Merging: Relationships")
        f = self.model.find(".//folder[@type='relations']", self.ns)

        for relationtype in source.relation2folder_dict:
            for relation in source.model.iterfind('.//element[@xsi:type="archimate:' + relationtype + '"]', self.ns):
                n = ElementTree.Element("element")
                if "name" in relation.attrib:
                    n.attrib["name"] = relation.attrib["name"]
                n.attrib["id"] = relation.attrib["id"]
                n.attrib["target"] = relation.attrib["target"]
                n.attrib["source"] = relation.attrib["source"]
                n.attrib["xsi:type"] = "archimate:" + relationtype

                f.append(n)
